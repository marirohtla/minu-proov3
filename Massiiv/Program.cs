﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiiv
{
    class Program
    {
        static void Main(string[] args)
        {
            //massiivi tähis=massiivi suurus

            int[] arvud = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 2 }; // massiivi algväärtustamise avaldis

            Console.WriteLine(arvud[7]);

            int[] arvukesed = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 2 }; //lihtsustus

            arvud[7] = 83; //massiivi index, mille väärtus on 83
            arvud[7]++;

            Console.WriteLine(arvukesed[4]);
        }

        int[][] jama = {
            new int[]{1, 2, 3 },
            new int[]{1, 2},
            new int[]{ 4, 7, 8},
    };

        int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            Console.WriteLine(jama[0][2]);
            Console.WriteLine(tabel[1,1]);

            Console.WriteLine (tabel.Rank);

     
       
    }

}
